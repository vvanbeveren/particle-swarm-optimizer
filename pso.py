#
# Multi-dimesional particle swarm optimizer
#
# Author: Vincent van Beveren <vvanbeveren@gmail.com>
#
# for usage, see examples below
#
import numpy as np


def ps_optimize(loss_func: callable,
                                min_space: tuple or float or int,
                                max_space: tuple or float or int,
                                w: float = 0.3,
                                la: float = 0.2,
                                ga: float = 0.1,
                                no_particles: int = 100,
                                max_iter: int = 100,
                                stop_loss: float = float('-inf'),
                                vectorize: bool = False,
                                return_all: bool = False,
                                custom_init: None or callable or np.array = None):
    """
    Dimension agnostic particle swarm optimizer. The optimizer attempts to minimize the loss resutling from the loss
    function. For each position the loss_func will be called with an equal number of arguments as there are dimensions.

    :param loss_func:       The function converting the position to a loss
    :param min_space:       Minimum coordinates of the vector space, must be a tuple or scalar
    :param max_space:       Maximum coordinates of the vector space, must be a tuple or scalar
    :param w:               Proportion of velocity vector updated to new iteration
    :param la:              Attraction to locally found (own) minimum
    :param ga:              Attraction to globally found minimum
    :param no_particles:    Number of particles in swarm
    :param max_iter:        Maximum number of iteration
    :param stop_loss:       Stop if loss is equal or below this value
    :param vectorize:       Vectorize the loss function (default is True)
    :param custom_init:     Use custom particle position initializer. Either provide a numpy array of shape
                            (no_particles, no_dims), with for each particle its position or a function. The function
                            will be called with the index of the particle number.
    :param return_all:      If True returns the best positions and respective loss values.
    :return:                When return_all is True return the best position and respective values for all particles.
                            Tuple of (best positions shape=(no_particles, no_dims), all loss shape(no_particles).
                            When return_all is False it only returns the best position and loss value.
                            Tuple of (best position, best loss).

    On each update the velocity of each update will be calculated as follows:
        vel(N+1) = w*vel(N) + la * (loc_best_pos-pos(N)) + ga * (global_best_pos-pos(N))
        pos(N+1) = pos(N) + vel(N+1)

    On vectorization:
        the loss function can be called with all particles at once. Set vectorize to False for this. The loss function will
        then be called with an 1D array for each dimension, containing the coordinate for that dimension per particle.

    A loss function for a 2-dimensional parabola which does not require vectorization:

        lambda x, y : x ** 2 * y ** 2
    """

    if vectorize:
        loss_func = np.vectorize(loss_func)

    if type(min_space) in (float, int):
        min_space = (min_space,)
    if type(max_space) in (float, int):
        max_space = (max_space,)

    if len(max_space) != len(min_space):
        raise ValueError("min_space and max_space not of same dimensionality")

    # derive dimensions
    no_dims = len(max_space)

    if custom_init is None:
        p_pos = np.stack(
            [np.random.rand(no_particles) * (mx-mn) + mn for mn, mx in zip(min_space, max_space)])
    elif type(custom_init) is np.array:
        if custom_init.shape != (no_dims, no_particles):
            raise ValueError("Expected custom_init array to be of shape ({},{})".format(no_dims, no_particles))
        p_pos = custom_init.transpose()
    elif callable(custom_init):
        p_pos = np.stack([custom_init(i) for i in range(no_particles)]).transpose()
        if p_pos.shape != (no_dims, no_particles):
            raise ValueError("Custom initializer function does not return vector of {} dimension(s)".format(no_dims))
    else:
        raise ValueError("custom_init of unknown type '{}'".format(type(custom_init)))

    p_vel = np.zeros(shape=(no_dims, no_particles))
    p_loss = loss_func(*p_pos)
    # Best is the current position
    p_best_pos = p_pos.copy()
    p_best_loss = p_loss.copy()

    best_p = p_best_loss.argmin()

    for i in range(max_iter):
        # Update position and velocity
        best_pos_exp = np.tile(p_best_pos[:, best_p], (no_particles, 1)).transpose()
        local_best_attr = la*np.random.random((no_dims, no_particles))*(p_best_pos-p_pos)
        global_best_attr = ga*np.random.random((no_dims, no_particles))*(best_pos_exp-p_pos)
        p_vel = w*p_vel + local_best_attr + global_best_attr
        p_pos += p_vel
        # Calculate new loss
        p_loss = loss_func(*p_pos)

        # select nodes for which to update losses
        update_p = np.nonzero(p_loss < p_best_loss)
        p_best_loss[update_p] = p_loss[update_p]
        p_best_pos[:, update_p] = p_pos[:, update_p]

        # and update the global loss
        best_p = p_best_loss.argmin()

        if p_best_loss[best_p] <= stop_loss:
            break

    if return_all:
        return p_best_pos.transpose(), p_best_loss
    else:
        return p_best_pos[:, best_p], p_best_loss[best_p]


if __name__ == "__main__":
    import matplotlib.pyplot as plt

    # Loss function
    def loss_func(x, y):
        return x ** 2 + (y + 0.5) ** 2 + np.sin(x*y*10)

    # Bounds of loss function
    min_space = (-1.5, -1)
    max_space = (1.5, 1)

    # Create image of loss function
    img_width = 150
    img_height = 100
    img = np.array([loss_func(x, y) for y in np.linspace(min_space[1], max_space[1], img_height)
                   for x in np.linspace(min_space[0], max_space[0], img_width)])
    img = img.reshape((img_height, img_width))

    # ===============================================================================================================
    # Simple usage
    # ===============================================================================================================
    pos, loss = ps_optimize(loss_func, min_space, max_space)
    plt.imshow(img, origin='lower', extent=(min_space[0], max_space[0], min_space[1], max_space[1]),
               interpolation='sinc')
    pos = pos.transpose()
    plt.plot([pos[0]], [pos[1]], "x", color="w")
    plt.title("Found minimum at {}\n with loss {}".format(pos, loss))
    plt.show()


    # ===============================================================================================================
    # Return and show all particles
    # ===============================================================================================================

    # Execute swarm optimization and return all positions (not that it will be unsorted)
    pos, loss = ps_optimize(loss_func, min_space, max_space, max_iter=20, return_all=True)

    # find best position
    best = np.argmin(loss)

    plt.imshow(img, origin='lower', extent=(min_space[0], max_space[0], min_space[1], max_space[1]),
               interpolation='sinc')
    posT = pos.transpose()
    plt.plot(posT[0], posT[1], ".", color="k")
    plt.plot([posT[0][best]], [posT[1][best]], "x", color="w")
    plt.title("All particles shown.\nFound minimum at {}\n with loss {}".format(pos[best], loss[best]))
    plt.show()

    # ===============================================================================================================
    # Custom initialization example (max_iter = 0 to show-off grid structure)
    # ===============================================================================================================
    pos, loss = ps_optimize(loss_func, min_space, max_space, no_particles=150, max_iter=0, return_all=True,
                                            custom_init=lambda x: ((x % 15)/5.0-1.4, (x // 15)/5.0-0.9))
    # find best position
    best = np.argmin(loss)

    plt.imshow(img, origin='lower', extent=(min_space[0], max_space[0], min_space[1], max_space[1]),
               interpolation='sinc')
    pos = pos.transpose()
    plt.plot(pos[0], pos[1], ".", color="k")
    plt.plot([pos[0][best]], [pos[1][best]], "x", color="w")
    plt.title("Custom initialization")
    plt.show()
