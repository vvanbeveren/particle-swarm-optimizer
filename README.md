# Particle Swarm Optimizer

A N-dimensional particle swarm optimizer in Python using Numpy.

**Simple 2D example**
```python
from pso import ps_optimize

loss_func = lambda x, y: x**2+y**2
pos, loss = ps_optimize(loss_func, (-1, -1), (1, 1))
print("Found solution at {} with loss {}".format(pos, loss))
```

**Simple 3D example**
```python
from pso import ps_optimize

loss_func = lambda x, y, z: x**2+y**2+z**2
pos, loss = ps_optimize(loss_func, (-1, -1, -1), (1, 1, 1))
print("Found solution at {} with loss {}".format(pos, loss))
```


More examples in the python source.
